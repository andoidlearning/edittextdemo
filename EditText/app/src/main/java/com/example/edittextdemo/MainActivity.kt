package com.example.edittextdemo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val firstNumber : EditText = findViewById(R.id.edt_firstNumber)
        val secondNumber : EditText = findViewById(R.id.edt_secondNumber)
        val btn : Button = findViewById(R.id.btnAdd)
        val textView : TextView = findViewById(R.id.tvResult)

        btn.setOnClickListener {
            val firstNum = firstNumber.text.toString().toInt()  
            val secondNum = secondNumber.text.toString().toInt()
            val result = firstNum + secondNum
            textView.text = result.toString()
        }
    }
}